package ch.bbw.MindMe.services;

import ch.bbw.MindMe.entities.Note;
import ch.bbw.MindMe.entities.StatDTO;
import ch.bbw.MindMe.repositories.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteService {

    @Autowired
    private NoteRepository repository;

    public List<Note> getAllNotes() {
        return repository.findAll();
    }

    public Note createNewNote(Note note) {
        return repository.save(note);
    }

    public void deleteNote(int id) {
        repository.deleteById(id);
    }

    public void setNoteDone(int id){
        Note noteToUpdate = repository.getById(id);
        noteToUpdate.setDone(true);
    }

    public StatDTO getStats() {
        StatDTO stats = new StatDTO();
        stats.setDone(repository.getDoneNotes());
        stats.setTodo(repository.getTodoNotes());
        return stats;
    }
}
