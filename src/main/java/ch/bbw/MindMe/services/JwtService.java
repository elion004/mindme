package ch.bbw.MindMe.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtService {

    public String createAuthenticationToken(Long id) {
        Algorithm algorithm = Algorithm.HMAC256("MindMeSecret");

        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + 3600000);

        return JWT.create()
                .withIssuer("MindMe")
                .withSubject(id.toString())
                .withIssuedAt(new Date())
                .withExpiresAt(expiryDate)
                .sign(algorithm);
    }
}