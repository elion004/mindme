package ch.bbw.MindMe.services;

import ch.bbw.MindMe.dtos.AuthRequestDTO;
import ch.bbw.MindMe.entities.User;
import ch.bbw.MindMe.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    private final Argon2PasswordEncoder encoder = new Argon2PasswordEncoder(16, 32, 1, 4096, 3);

    public User createNewUser(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        return repository.save(user);
    }

    public User loginUser(AuthRequestDTO authRequest) {
        User user = repository.findByEmail(authRequest.getEmail());
        if (encoder.matches(authRequest.getPassword(), user.getPassword())) {
            return user;
        } else {
            throw new RuntimeException("E-Mail and Password don't match");
        }
    }
}
