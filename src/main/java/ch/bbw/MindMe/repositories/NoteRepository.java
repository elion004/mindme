package ch.bbw.MindMe.repositories;

import ch.bbw.MindMe.entities.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface NoteRepository extends JpaRepository<Note, Integer> {

    @Query("SELECT COUNT(n.id) FROM Note n WHERE n.done = true")
    int getDoneNotes();

    @Query("SELECT COUNT(n.id) FROM Note n WHERE n.done = false")
    int getTodoNotes();
}
