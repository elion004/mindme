package ch.bbw.MindMe;

import ch.bbw.MindMe.controllers.NoteController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MindMeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MindMeApplication.class, args);
    }

    @Autowired
    NoteController controller;

}
