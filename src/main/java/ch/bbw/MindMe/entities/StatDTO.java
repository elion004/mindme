package ch.bbw.MindMe.entities;

import lombok.Data;

@Data
public class StatDTO {

    private int done;
    private int todo;

    public int getDone() {
        return done;
    }

    public void setDone(int done) {
        this.done = done;
    }

    public int getTodo() {
        return todo;
    }

    public void setTodo(int todo) {
        this.todo = todo;
    }
}
