package ch.bbw.MindMe.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "users")
public class User {

    private final static String regex = "\\b([A-ZÀ-ÿ][-,a-z. ']+[ ]*)+";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Name must not be empty")
    @Pattern(regexp = regex, message = "Character not allowed")
    private String name;

    @NotBlank(message = "Lastname must not be empty")
    @Pattern(regexp = regex, message = "Character not allowed")
    private String Lastname;

    @Column(unique = true)
    @NotBlank(message = "Email must not be empty")
    @Email(message = "Not a valid Email")
    private String email;

    @NotBlank(message = "Password must not be empty")
    @Size(min = 5, message = "Password must be at least 5 characters")
    private String password;

    @OneToMany
    private List<Note> notes;

}
