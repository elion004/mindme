package ch.bbw.MindMe.dtos;

import lombok.Data;

@Data
public class LoginResponseDTO {

    public LoginResponseDTO(String JWT) {
        this.JWT = JWT;
    }

    private String JWT;

}
