package ch.bbw.MindMe.controllers;

import ch.bbw.MindMe.entities.Note;
import ch.bbw.MindMe.services.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class NoteController {

    @Autowired
    private NoteService service;

    @GetMapping("/home")
    public String getAll(Model model) {
        model.addAttribute("notes", service.getAllNotes());
        return "home";
    }

    @GetMapping("/stats")
    public String getStats(Model model) {
        model.addAttribute("stats", service.getStats());
        return "stats";
    }

    @GetMapping("/create")
    public String getCreateForm(Model model) {
        model.addAttribute("createForm", new Note());
        return "create";
    }

    @PostMapping("/create")
    public String create(@Valid Note note, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "create";
        }
        note.setId(service.getAllNotes().size());
        service.createNewNote(note);
        return "home";
    }

    @PostMapping("/home")
    public String update(@ModelAttribute Note note, Model model, @PathVariable int id) {
        model.addAttribute("updateNote", new Note());
        service.setNoteDone(id);
        return "home";
    }


    @DeleteMapping("/home/{id}")
    public String delete(@PathVariable int id) {
        service.deleteNote(id);
        return "home";
    }

// These controllers were used for the database connection and authorization
// but not for the finished product
//    @Autowired
//    private NoteService service;
//
//    @GetMapping("/note")
//    private ResponseEntity<List<Note>> allNotes() {
//        return new ResponseEntity<>(service.getAllNotes(), HttpStatus.OK);
//    }
//
//    @PostMapping("/note")
//    private ResponseEntity<Note> createNote(@RequestBody Note note) {
//        return new ResponseEntity<>(service.createNewNote(note), HttpStatus.ACCEPTED);
//    }
//
//    @DeleteMapping("/note/{id}")
//    private ResponseEntity<?> deleteNote(@PathVariable int id) {
//        service.deleteNote(id);
//        return ResponseEntity.ok("Note delete");
//    }

}
