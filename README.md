# MindMe

## 1. Idee

Es solle eine Web-Applikation entstehen in der man, Notizen erstellen und löschen kann.

Optional sollte die Applikation mit JPA an eine H2 Datenbank verbunden werden. Damit wäre es möglich einen Account zu
erstellen und darin seine Notizen zu speichern.

## 2. Layout Skizze

Hier haben wir das grobe Layout. Ganz simple mit Header, dann der Content und zum Schluss noch ein Footer.

![img_3.png](img_3.png)

## 3. GUI Skizze
Hier haben wir die Home-Page. Hier hat der User die Übersicht über seine 
offenen Tasks.

![img_2.png](img_2.png)

Hier haben wir die Stats-Page. Hier hat der User eine Übersicht über wie viel Tasks noch offen sind
und welche noch zu erledigen sind.

![img_1.png](img_1.png)

Hier haben wie die Add-Page. Hier kann der User ein neuen Task erstellen.

![img.png](img.png)


## 4. Use Cases

Use cases wurden auf GitLab dokumentiert: https://gitlab.com/elion004/mindme

## 5. Fazit

Aufgrund konnte von Zeitmangel konnte die Authorization nur unvollständig implementiert werden.

Punkte die Provisorisch implementiert wurden:

* H2-Datenbankverbindung
* User-Entity
* Erstellen von usern
* Login von usern
* Generieren vom JWT Token

Gescheitert an der Authorization an sich.